<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class ListClassificationTest extends TestCase
{
    public function testListClassificationGeneral()
    {
        $response = $this->get('/api/results/');

        $response->assertStatus(200);
    }

    public function testListClassificationPerAge()
    {
        $response = $this->get('/api/results/?group-by-age=true');

        $response->assertStatus(200);
    }
}
