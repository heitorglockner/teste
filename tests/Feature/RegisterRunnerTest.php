<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class RegisterRunnerTest extends TestCase
{
    public function testRegisterRunner()
    {
        $faker = \Faker\Factory::create('pt_BR');

        $response = $this->withHeaders([
            'Accept' => 'application/json',
            'Content-Type' => 'application/json',
        ])->json('POST', '/api/runners', [
            'name' => $faker->name,
            'document' => $faker->cpf,
            'date_birth' => $faker->dateTimeBetween('-99 years', '-18 years')->format('d/m/Y')
        ]);

        $response
            ->assertStatus(200)
            ->assertJson([
                'response' => 'Corredor incluído com sucesso.',
            ]);
    }
}
