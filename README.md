### Instalação do projeto
docker exec -it deliverit-app composer install \
docker exec -it deliverit-app cp .env.example .env \
docker exec -it deliverit-app php artisan migrate \
docker exec -it deliverit-app php artisan db:seed

### Exemplos de requisições (postman)
https://www.getpostman.com/collections/4e5e8506b72c481659c4


