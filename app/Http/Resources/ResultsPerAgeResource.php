<?php

namespace App\Http\Resources;

use App\Models\Result;
use Carbon\Carbon;
use Illuminate\Http\Resources\Json\JsonResource;

class ResultsPerAgeResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $results = Result::where('competition_id', $this->resource->id)
            ->select('ru.id', 'ru.name', 'ru.date_birth', 'results.time_start', 'results.time_end')
            ->join('runners AS ru', 'ru.id', 'results.runner_id')
            ->orderBy('date_birth', 'DESC')
            ->get()
            ->map(function($result)
            {
                $result->duration = $result->time_start->diff($result->time_end)->format('%H:%I:%S');
                $result->age = Carbon::parse($result->date_birth)->diff(Carbon::now())->format('%y');
                return $result;
            });

        $results = collect($this->groupResultsByAge($results));

        return [
            'id_prova' => $this->resource->id,
            'tipo_de_prova' => "{$this->resource->type}km",
            'resultados' => $results,
        ];
    }

    /**
     * @param $results
     * @return array
     */
    private function groupResultsByAge($results)
    {
        $ageGroups = [
            '18 - 24 anos',
            '25 - 34 anos',
            '35 - 44 anos',
            '45 - 54 anos',
            '55 anos ou mais'
        ];

        $formattedGroupsPerAge = [];
        $groupsPerAge = $this->groupResultByAge($results);

        for ($i = 0; $i <= 4; $i++) {
            if (isset($groupsPerAge[$i])) {
                $formattedGroupsPerAge[$i]['classificacao'] = $this->formatRunnersInGroup($i, $groupsPerAge);
                $formattedGroupsPerAge[$i]['grupo'] = $ageGroups[$i];
            }
        }

        return $formattedGroupsPerAge;
    }

    /**
     * @param $groupIndex
     * @param $groupsPerAge
     * @return \Illuminate\Support\Collection
     */
    private function formatRunnersInGroup($groupIndex, $groupsPerAge)
    {
        $position = 1;
        return collect($groupsPerAge[$groupIndex])
            ->map(function ($group) use (&$position) {
                return [
                    'id_corredor' => $group['id'],
                    'idade' => $group['age'],
                    'nome_corredor' => $group['name']
                ];
            })
            ->sortBy('duration')
            ->map(function ($group) use (&$position) {
                $group['posicao'] = $position++;
                return $group;
            })
            ->values();
    }

    /**
     * @param $results
     * @return array
     */
    private function groupResultByAge($results) {
        $groupsPerAge = [];

        foreach ($results as $result) {
            switch (true) {
                case $result->age <= 24:
                    $groupsPerAge[0][] = $result->toArray();
                    break;
                case $result->age <= 34:
                    $groupsPerAge[1][] = $result->toArray();
                    break;
                case $result->age <= 44:
                    $groupsPerAge[2][] = $result->toArray();
                    break;
                case $result->age <= 54:
                    $groupsPerAge[3][] = $result->toArray();
                    break;
                default:
                    $groupsPerAge[4][] = $result->toArray();
                    break;
            }
        }

        return $groupsPerAge;
    }
}
