<?php

namespace App\Http\Resources;

use App\Models\Result;
use Carbon\Carbon;
use Illuminate\Http\Resources\Json\JsonResource;

class ResultsResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $results = Result::where('competition_id', $this->resource->id)
            ->select('ru.id', 'ru.name', 'ru.date_birth', 'results.time_start', 'results.time_end')
            ->join('runners AS ru', 'ru.id', 'results.runner_id')
            ->get()
            ->map(function($result)
            {
                $result->duration = $result->time_start->diff($result->time_end)->format('%H:%I:%S');
                $result->age = Carbon::parse($result->date_birth)->diff(Carbon::now())->format('%y');
                return $result;
            });

        return [
            'id_prova' => $this->resource->id,
            'tipo_de_prova' => "{$this->resource->type}km",
            'resultados' => $this->formatRunners($results),
        ];
    }

    /**
     * @param $groupIndex
     * @param $groupsPerAge
     * @return \Illuminate\Support\Collection
     */
    private function formatRunners($results)
    {
        $position = 1;
        return $results->map(function ($group) {
                return [
                    'id_corredor' => $group['id'],
                    'idade' => $group['age'],
                    'nome_corredor' => $group['name']
                ];
            })->sortBy('duration')
            ->map(function ($group) use (&$position) {
                $group['posicao'] = $position++;
                return $group;
            });
    }
}
