<?php

namespace App\Http\Controllers;

use App\Http\Requests\ResultsRequest;
use App\Http\Resources\ResultsPerAgeResource;
use App\Http\Resources\ResultsResource;
use App\Models\Competition;
use App\Models\Result;
use Illuminate\Http\Request;

class ResultsController extends Controller
{
    /**
     * @param ResultsRequest $request
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\Routing\ResponseFactory|\Illuminate\Http\Response
     */
    public function store(ResultsRequest $request)
    {
        $input = $request->all();

        Result::create($input);

        return response([
            'response' => 'Resultado incluído com sucesso.'
        ], 200);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
     */
    public function index(Request $request)
    {
        $results = Result::select(['c.id', 'c.type'])
            ->join('competitions AS c', 'c.id', 'results.competition_id')
            ->groupBy('c.id')
            ->get();

        return $request->input('group-by-age') ? ResultsPerAgeResource::collection($results) : ResultsResource::collection($results);
    }
}
