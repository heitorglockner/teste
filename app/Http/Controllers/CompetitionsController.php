<?php

namespace App\Http\Controllers;

use App\Http\Requests\CompetitionsRequest;
use App\Http\Requests\CompetitionsRunnersRequest;
use App\Models\Competition;
use Carbon\Carbon;

class CompetitionsController extends Controller
{
    protected $dates = [
        'date',
    ];

    public function store(CompetitionsRequest $request)
    {
        $input = $request->all();
        $input['date'] = Carbon::createFromFormat('d/m/Y', $input['date'])->format('Y-m-d');

        Competition::create($input);

        return response([
            'response' => 'Corrida incluída com sucesso.'
        ], 200);
    }

    public function storeRunnerInCompetition(CompetitionsRunnersRequest $request)
    {
        $competition = Competition::find($request->competition_id);
        $competition->runner()->attach($request->runner_id);

        return response([
            'response' => 'Corredor incluído na corrida com sucesso.'
        ], 200);
    }
}
