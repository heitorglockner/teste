<?php

namespace App\Http\Controllers;

use App\Http\Requests\RunnersRequest;
use App\Models\Runner;
use Carbon\Carbon;

class RunnersController extends Controller
{
    protected $dates = [
        'date_birth',
    ];

    public function store(RunnersRequest $request)
    {
        $input = $request->all();
        $input['date_birth'] = Carbon::createFromFormat('d/m/Y', $input['date_birth'])->format('Y-m-d');

        Runner::create($input);

        return response([
            'response' => 'Corredor incluído com sucesso.'
        ], 200);
    }
}
