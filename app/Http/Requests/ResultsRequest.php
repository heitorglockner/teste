<?php

namespace App\Http\Requests;

use App\Rules\RunnerExistsOnCompetition;
use Illuminate\Foundation\Http\FormRequest;

class ResultsRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'runner_id' => ['required', 'exists:runners,id'],
            'competition_id' => ['required', 'exists:competitions,id', new RunnerExistsOnCompetition($this->runner_id)],
            'time_start' => ['required', 'date_format:H:i'],
            'time_end' => ['required', 'date_format:H:i', 'after:time_start']
        ];
    }
}
