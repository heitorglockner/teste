<?php

namespace App\Http\Requests;

use App\Rules\RunnerWithoutCompetitionsOnDay;
use Illuminate\Foundation\Http\FormRequest;

class CompetitionsRunnersRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'runner_id' => ['required', 'exists:runners,id', new RunnerWithoutCompetitionsOnDay($this->competition_id)],
            'competition_id' => ['required', 'exists:competitions,id']
        ];
    }
}
