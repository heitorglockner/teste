<?php

namespace App\Http\Requests;

use App\Rules\CPF;
use App\Rules\Over18Years;
use Illuminate\Foundation\Http\FormRequest;

class RunnersRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => ['required'],
            'document' => ['required', new CPF],
            'date_birth' => ['required', 'date_format:"d/m/Y', new Over18Years]
        ];
    }
}
