<?php

namespace App\Rules;

use App\Models\Competition;
use App\Models\Runner;
use Illuminate\Contracts\Validation\Rule;

class RunnerWithoutCompetitionsOnDay implements Rule
{
    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct($competitionId)
    {
        $this->competition_id = $competitionId;
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        $competition = Competition::find($this->competition_id);
        if (empty($competition)) {
            return false;
        }

        $runner = Runner::with('competition')->find($value);
        if (empty($runner)) {
            return false;
        }

        $competitionsOnDay = Competition::where('date', $competition->date)->pluck('id')->toArray();
        $runnerParticipateOnDay = $runner->competition()->whereIn('id', $competitionsOnDay)->count();

        if ($runnerParticipateOnDay > 0) {
            return false;
        }

        return true;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'Corredor já possui corridas na data.';
    }
}
