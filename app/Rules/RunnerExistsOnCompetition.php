<?php

namespace App\Rules;

use App\Models\Runner;
use Illuminate\Contracts\Validation\Rule;

class RunnerExistsOnCompetition implements Rule
{
    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct($runnerId)
    {
        $this->runner_id = $runnerId;
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        $runner = Runner::find($this->runner_id);
        if (empty($runner)) {
            return false;
        }

        return $runner->competition()->where('competition_id', $value)->exists();
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'Corredor não está inscrito na corrida.';
    }
}
