<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCompetitionRunnerTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('competition_runner', function (Blueprint $table) {
            $table->unsignedInteger('runner_id');
            $table->unsignedInteger('competition_id')->index('competitions_runners_FK');
            $table->unique(['runner_id', 'competition_id'], 'competitions_runners_runner_id_IDX');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('competition_runner');
    }
}
