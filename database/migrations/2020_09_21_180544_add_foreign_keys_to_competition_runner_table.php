<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddForeignKeysToCompetitionRunnerTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('competition_runner', function (Blueprint $table) {
            $table->foreign('competition_id', 'competitions_runners_FK')->references('id')->on('competitions')->onUpdate('RESTRICT')->onDelete('RESTRICT');
            $table->foreign('runner_id', 'competitions_runners_FK_1')->references('id')->on('runners')->onUpdate('RESTRICT')->onDelete('RESTRICT');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('competition_runner', function (Blueprint $table) {
            $table->dropForeign('competitions_runners_FK');
            $table->dropForeign('competitions_runners_FK_1');
        });
    }
}
