<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddForeignKeysToResultsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('results', function (Blueprint $table) {
            $table->foreign('competition_id', 'classification_FK')->references('id')->on('competitions')->onUpdate('RESTRICT')->onDelete('RESTRICT');
            $table->foreign('competition_id', 'classification_FK_1')->references('id')->on('competitions')->onUpdate('RESTRICT')->onDelete('RESTRICT');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('results', function (Blueprint $table) {
            $table->dropForeign('classification_FK');
            $table->dropForeign('classification_FK_1');
        });
    }
}
