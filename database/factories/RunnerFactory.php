<?php

namespace Database\Factories;

use App\Models\Runner;
use Faker\Provider\pt_BR\Person;
use Illuminate\Database\Eloquent\Factories\Factory;

class RunnerFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Runner::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        $faker = \Faker\Factory::create('pt_BR');

        return [
            'name' => $faker->name,
            'document' => $faker->cpf,
            'date_birth' => $faker->dateTimeBetween('-99 years', '-18 years')
        ];
    }
}
