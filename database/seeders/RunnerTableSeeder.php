<?php

namespace Database\Seeders;

use App\Models\Runner;
use Illuminate\Database\Seeder;

class RunnerTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Runner::factory()->count(50)->create();
    }
}
