<?php

use App\Http\Controllers\CompetitionsController;
use App\Http\Controllers\ResultsController;
use App\Http\Controllers\RunnersController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/


Route::resource('runners', RunnersController::class)->only(['store']);
Route::post('competitions/runner/store', [CompetitionsController::class, 'storeRunnerInCompetition']);
Route::resource('competitions', CompetitionsController::class)->only(['store']);
Route::resource('results', ResultsController::class)->only(['store', 'index']);
